# Ansible util

Docker image for running ansible and associated useful utilities.

Currently only includes [sops](https://github.com/mozilla/sops) in addition to ansible.

This image also removes certain annoyingly large and/or oldfashioned ansible modules provided with the opensuse ansible package by default. See Dockerfile for details.
## Usage

`docker run registry.gitlab.com/oivindoh/ansible-util:latest ansible-playbook git-checkout-path/playbooks/playbook.yml`

### Environment variables

Environment variables can be used to supply SSH and GPG keys for use by ansible and sops.

All variables in a set must be supplied in order to use that set.

| variable | set | 
| --- | --- |
| GPG_KEY | gpg | GPG private key used to decrypt sops secret files |
| GPG_PASS | gpg | Password for GPG private key |
| SSH_PRIV_KEY | ssh | Private key for SSH connections |
| SSH_PUB_KEY | ssh | Public key for SSH connections |
| SSH_CONFIG | ssh | SSH configuration (e.g. jumphost config, user config) |