#!/bin/bash

function log_message()
{
    timestamp=$(date +'%Y-%m-%d %H:%M:%S')
    message=$1
    level=${2:-'-'}

    echo "[${level}] ${timestamp} -- ${message}"
}

function log_change()
{
    log_message "$1" '~'
}
function log_warn()
{
    log_message "$1" '!'
}

function setup_gpg()
{
    log_message "GPG Setup"
    if [[ -v GPG_KEY && -v GPG_PASS ]]; then
        log_change "Importing ${#GPG_KEY}-lenght key and ${#GPG_PASS}-length passphrase for unlock"
        echo "$GPG_KEY" | gpg --batch --import
        echo $GPG_PASS | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s $(mktemp)
    else
        log_warn "No key/pass supplied. Supply GPG_KEY and GPG_PASS environment variables for import"
    fi
}

function setup_ssh()
{
    log_message "SSH Setup"
    if [[ -v SSH_PRIV_KEY && -v SSH_PUB_KEY && -v SSH_CONFIG ]]; then
        log_change "Placing ${#SSH_PRIV_KEY}-length key, ${#SSH_PUB_KEY}-length pubkey and ${#SSH_CONFIG}-length SSH config"
        mkdir -p /root/.ssh
        echo "$SSH_PRIV_KEY" > /root/.ssh/id_rsa
        echo "$SSH_PUB_KEY" > /root/.ssh/id_rsa.pub
        echo "$SSH_CONFIG" > /root/.ssh/config
        chmod 0600 /root/.ssh/id_rsa
    else
        log_warn "No SSH config supplied. Supply SSH_PRIV_KEY, SSH_PUB_KEY and SSH_CONFIG environment variables"
    fi
}

if [[ -v $CI_PROJECT_DIR ]]; then
    log_change "Changing directory to ${CI_PROJECT_DIR}"
    cd $CI_PROJECT_DIR
fi

setup_gpg
setup_ssh

log_message "Ready for action"

exec "$@"