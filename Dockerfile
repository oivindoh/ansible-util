ARG architecture="arm64"
ARG sopsversion="3.7.2"
ARG suseversion="15.4"

FROM opensuse/leap:${suseversion}
RUN zypper -n install ansible openssh-clients gpg2 && rm -rf /var/cache/zypp \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/fortios ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/f5 ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/cloudengine ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/cloud/amazon ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/cloud/azure ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/cloud/google ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/cloud/vmware ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/cloud/ovirt ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/cloud/cloudstack ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/aci ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/nxos ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/fortimanager ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/check_point ||true \
    && rm -rf /usr/lib/python3.6/site-packages/ansible/modules/network/avi ||true \
    && ls /usr/lib/python3.6/site-packages/babel/locale-data/ | grep -v -E 'en_(GB|US).dat' | xargs -I{} rm /usr/lib/python3.6/site-packages/babel/locale-data/{}
ARG sopsversion
ARG architecture
ADD https://github.com/mozilla/sops/releases/download/v${sopsversion}/sops-v${sopsversion}.linux.${architecture} /usr/local/bin/sops
RUN chmod 755 /usr/local/bin/sops
ADD ./includes/entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
